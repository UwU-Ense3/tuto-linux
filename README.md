E3 uwu


Projet : Robotique en essaim
============

**Introduction :**
Le but de ce tuto est de maitriser les base de linux ce qui vous permettra de passer un bon moment si vous devez utiliser l'invite de commande.
Pour commencer il vous faut un ordinateur avec un os linux. pour cela nous avons une vidéo qui vous explique comment installer une machine virtuelle: https://www.youtube.com/watch?v=ubZLAU0EnE0

Une fois que vous vous etes procuré un système linux de votre choix, vous pouvez l'utiliser comme windows avec l'interface visuel. 
Cepend
ant pour la programmation, nous allons nous interesser à l'invite de commande. Vous le trouverez dans le menu demarer en haut à gauche.
Vous le trouverez sous le nom invite de commante, ou terminal.

C'est avec cette interface que l'on va travailler.
Pour ce tuto nous allons illustrer comment editer un programme.

** Editer vos programmes:**

Le terminal linux vous permet de vous deplacer dans les dossier d'en creer, d'en suprimer, et d'editer des fichier.

Pour commencer l'interface se présente sous la forme d'une fenetre noir dans laquelle vous pouvez écrire. Cellon le système que vous utilisez la mise en forme peut être differente.
Pour la machine virtuelle vous avez le symbole ```>``` suivit du curseur (acrré blanc clignotant).
Pour un système  ubuntu vous avez une ligne du type ```ordi_tot[~]:``` ordi_toto indique le nom de votre machine ( le nom de la machine qui execute la commande) et entre crochet le dossier où vous  êtes, ici ~ signifie le dossier home (le dossier utilisateur).
Dans les deux cas vous pouvez taper des lignes de commande si vous avez le curseur blanc d'affiché.

Pour connaitre le dossier où vous etes il faut taper la commande ```pwd```  Pour ma part l'ordinateur me retourne la ligne:

```/home/toto```

Vous vous  trouvez donc dans le dossier utilisateur toto qui est rangé dans home.

Maintenant si vous voulez savoir ce qu'il y a dans le dossier vous pouvez taper la commande ```ls```
l'ordinateur vous retourne 

```Bureau Document Downloads Images Modèles Musique Public Vidéos```

On peut voir les dossier en bleu et les fichier en blanc (il peut y avoir d'autres type d'element).

Nous allons maintenant creer un dossier:
Pour cela nous allons mettre la commande ```mkdir Mes_programmes_python```

ATTENTION il faut eviter à tout prix eviterde mettre des espace dans les noms de dossier.

Si on tape à nouveau ```ls``` on a la ligne:

```Bureau Document Downloads Images Mes_programmes_python Modèles Musique Public Vidéos```

L'historique des commandes vous permet de voir ce que vous avez fait.
Pour deplacer dans ce repertoire vous devez taper la ligne:

```cd Mes_programmes_python```

cd signifie change directory.
Pour aller plus vite taper le debut du nom du dossier et ensuite avec tab il y a l'auto completion.
(Si il y a plusieurs possibilités pour completer, tapez deux fois sur tab pour voir les possibilitées).
ATTENTION il est important de maitriser la touche tab.

Pour revenir au dossier precedent  (pour "remonter" d'un dossier) il faut taper la commande ```cd ../```

```../``` signifie revenir en arrière. Et on peut cumuler le chemin par exemple:```cd ../Document``` signifi reculer d'un dossier puis aller dans le dossier document. On peut mettre une adresse plus complexe avec ```/``` entre chaque dossier.
La commande cd sans rien  derière vous fait revenir au dossier utilisateur (retour au point de depart).


Maintenant nous allons creer un dossier Mes_programmes_C et aller dans ce repertoire:

```mkdir Mes_programmes_C```

```cd Mes_programmes_C```

```ls```

Vous avez donc un dossier vide le but maintenant va etre d'ecrire un programme C.

Il y a plusisuers manière de le faire nus allons aller de la plus simple (la moins pratique) à la plus avancée.

Pour commencer: gedit:

La commande gedit permet d'editer un fichier avec l'equivalent du bloc-note. Si le fichier exite pas il le creer.
C'est l'extension du fichier qui permet de connaitre le langage de programmation.

tapez la commande: ```gedit exemple.c```
une fenaitre s'ouvre vous allez pouvoir écrire votre code (si vous n'avez pas encore eu les cours de C, il vous suffit de suivre les consignes).
```

#include <stdlib.h>
#include <stdio.h>

int main()
{
    printf("Hello, world!\nUwU\n");
    return 0;
}
```

enregistrez votre travail, et fermez la fenetre.

ensuite vous devez compiler le code:

```gcc -o exemple exemple.c```

Puis executez le programme:

```./exemple```

Et le terminal vous affiche le "shell" avec Hello world.

Bon on peut constater que cette methode n'est pas la plus pratique.

à l'aide des commandes rendez vous dans le dossier Mes_programmes_python.


nous allons utiliser l'IDE VS code, mais il est possible d'en utiliser un autre comme spider, pyzo ...


Pour commencer il faut installer l'ide (si c'est pas déjà fait)

Pour installer VS code vous devez saisir la commande:

```sudo snap install --classic code```

VS code vous permet d'innstaller des module et modifier votre environement de travail pour qu'il corresponde à vos attente. (cf documentation VS code)

Vous pouvez aussi l'installer avec le __gestionnaire des application__ linux ou windows (enfonction de votre installation) (c'est parfois plus facile).

Une fois que vous avez installé votre VS code avec la methode de votre choix, nous allons installer Python.
Vs code a beaucoup d'outils pour vous aider. mais cela fait l'objet d'un autre tuto.

En cas de besoin d'aide je reste à votre diposition.

Dans notre cas nous restons dans l'invite de commande vlinux.

Il faut d'abord vérifier si python est deva installé:

```python --version``` ou  ```python3 --version```

Si il affiche la version de python installé alors vous pouvez à la suite.
Sinon, saisissez la commande ``` sudo apt-get install python``` pour installer python.

Pour editer votre fichier python avec VS code: ```code test.py```

tapez le code:

```
print("Hello world.")
print("UwU!")

```

vous pouvez customiser les couleurs, vous pouvez aussi executer via l'interface... voir le tuto VS code.

Dans notre cas apres avoir enregistré je vous sugère de saisir dans le terminal la commande: ```python test.py``` ou ```python3 test.py```

L'exemple est trés simpliste mais je vous laisse vous exercer sur vos projet pour mieux maitriser l'interface.

La dernière methode est d'utiliser Vim. 
Pour maitriser cet outil il faut suivre le tuto: https://openclassrooms.com/fr/courses/43538-reprenez-le-controle-a-laide-de-linux/42693-vim-lediteur-de-texte-du-programmeur



** Manipulation des fichiers et dossier:**

Il y a une série de commande pour manipuler vos fichier et vos dossier:


```rm nom_du_fichier``` vous permet de supprimer votre fichier ( attention il y a pas de corbeille).
Le terminal vous demande une confirmation avant de supprimer (y pour yes et n pour no).
Pour supprimer un dossir:

```rm -r nom_du_dossier``` -r signifie récursif donc il va supprimer le dossier mais également ce qu'il contient.
Mais il va vous demander la validation pour chaque élément.

```rm -f nom_du_fichier``` -f veut dire sans comnfirmation, il va supprimer le fichier sans vous demander de valider.

```rm -rf nom_du_dossier``` on peut cumuler donc ici on supprime de manière recursif sans validation.

Pour supprimer plusieurs fichiers vous pouvez utiliser ```*``` qui va selectionner tout les fichiers qui contienne l'extrait:

```rm *.py ``` permet donc de supprimer tous les fichiers python dans le répértoire.

ATTENTION ```rm -rf * ``` va supprimer tous les fichier de manière récursif sans validation c'est donc une commande à éviter.

Pour déplacer ou renomer il faut utilser la comande mouve:```mv```

```mv nom1 nom2``` permet de renommer le fichier.

```mv nom1 dossier/nom1``` permet de deplacer le fichier dans le dossier.

De même la commande cp permet de copier un fichier ou dossier.

Pour ouvir un fichier comme un pdf ou un .docs:

```xdg-open nom_du_fichier``` permet d'ouvrir le fichier avec le logiciel par defaut (comme si vous louvriez avec l'explorateur de fichier).

```xdg-open .``` permet d'ouvrir l'explorateur des fichier à l'endroit où vous etes.

** Manipulation les programme en execution:**


```ctrl+c``` permet __d'interrompre__ la commande en cours ce qui est utile si votre programme plante ou ne termine pas.

```ctrl+d``` permet de se deconnecter par exemple si vous le controle d'une machine à distance ou meme pour fermer le terminal en cours.

Quand vous faite un gedit un xdg ou code, le terminal est bloqué le temps que la fentreest ouverte pour éviter cela utiliser ```&```:

```gedit test.py &``` permet d'ouvrir en arriere plan l'editeur.

Pour executer un programme sans avoir le retour (en arriere plan) vous devez utiliser nohup:

```nohup python3 test.py &``` permet de lancer le programme python en tâche de fond, et la sortie du programme est notée dans fichier nohup.out (fichier texte).

```top``` permet d'afficher les programme en cours d'execution.
Vous pouvez tuer un programme avec kill suivit de l'identifiant du programme.

la commande ll vous permet de voir les detaile sur les fichier (ls amélioré).
Dans la commande de droite on peut voir les autorisation en écriture lecture et execution, suivit du propriétaire du fichier, et de son volume.
Vous pouvez modifier les droit avec la commande chmod. pour plus d'info: https://doc.ubuntu-fr.org/permissions


**Gestion de telechargement et de compression:**

Télécharger simplement un fichier :

```wget http://www.site.org/rep/01/fichier.txt```

Télécharger sur un FTP avec authentification (ici le nom d'utilisateur est paul et son mot de passe 123) :

```wget -r l4 ftp://paul:123@serveur.org/```

Spécifier un dossier de destination :

```wget -P $HOME/dossier/de/destination http://www.site.org/rep/01/fichier.txt```


plus d'infos  dans la doc: https://doc.ubuntu-fr.org/wget

Pour compresser une dossier on utilse le format .tar (zip c'est pour windows).

Doc pour utiliser ```tar``` : https://doc.ubuntu-fr.org/tar

(Pour les zip: https://doc.ubuntu-fr.org/zip)

**Pour faire une recherche:**

find permet de trouver un fichier:

```find -r -name *.py``` permet de chercher de manière récursive le fichier dont le nom termine par .py.

Pour chercher un mot dans les fichier d'un dossier on utilise: ```grep mot_à_chercher```.

**Pour aller plus loin:**

Pour creer un lien: https://fr.wikipedia.org/wiki/Ln_(Unix)#:~:text=La%20commande%20Unix%20ln%20permet,l'option%20par%20d%C3%A9faut).

Pour modifier le bashrc: https://wiki.visionduweb.fr/index.php/Le_fichier_.bashrc

Pipe: https://doc.ubuntu-fr.org/pipe


Screen: https://doc.ubuntu-fr.org/screen

Tmux: https://doc.ubuntu-fr.org/tmux

Midnight Commander: https://doc.ubuntu-fr.org/midnight_commander

ATTENTION pour copier coller un ligne dans le terminal, sur linux il faut utiliser le click molette.
